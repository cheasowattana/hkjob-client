import React from 'react'
import LoginForm from '../components/loginForm'

import './login.css'

class Login extends React.Component {
    render(){
        return (
            <div className="login-container">
                <h2>Login</h2>
                <LoginForm />
            </div>
        );
    }
}

export default Login;