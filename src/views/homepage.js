import React from 'react'
import HomePage from '../components/homePage'

import './homepage.css'

class Homepage extends React.Component {
    render(){
        return (
            <div className="homepage-container">
                <HomePage />
            </div>
        );
    }
}

export default Homepage;