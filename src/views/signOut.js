import React from 'react';
import { Redirect } from 'react-router-dom';

class SignOut extends React.Component {

    componentWillMount(){
        if(this.props.isAuthenticated) {
            localStorage.clear();
        }
    }

    render(){
        return (
            <Redirect to="/" />
        );
    }

}

export default SignOut;