import React from 'react'

import './signUp.css'

import Form from '../components/signupForm'

class SignUp extends React.Component {
    
    render(){
        return (
            <div className="signup-page">
                <h2>Sign Up</h2>
                <Form />
            </div>
        );
    }

}

export default SignUp;