import React from 'react'
import { Router, Route, Switch } from 'react-router-dom'

import history from './history'
import { ProtectedRoute } from './protectedRoute'
import checkLocalStorage from './checkLocalStorage'

import SignUp from './views/signUp'
import SignOut from './views/signOut'
import Login from './views/login'
import HomePage from './views/homepage'

export default (
    <Router history={ history }>
        <Switch>
            <ProtectedRoute isAllowed={ checkLocalStorage() } path="/homepage" render={ props => <HomePage { ...props } /> } />
            <ProtectedRoute isAllowed={ checkLocalStorage() } path="/logout" render={ props => <SignOut { ...props } /> } />
            <Route path="/signup" title="Sign Up" render={ props => <SignUp { ...props } /> }  />
            <Route exact path="/" title={ "Login" } render={ props => <Login { ...props } /> } />
        </Switch>
    </Router>
)