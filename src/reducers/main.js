import { combineReducers } from 'redux'
import profile from './profile'
import auth from './auth'

const mainReducer = combineReducers({
    profile,
    auth
});

export default mainReducer;