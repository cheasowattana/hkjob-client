import * as LoginActions from '../actions/auth'

const loginReducer = ( state = {}, actions ) => {
    switch( actions.type ){
        case LoginActions.LOGIN_REQUEST: return { ...state, ...actions.isAuthenticated }
        default: return state;
    }
}

export default loginReducer;