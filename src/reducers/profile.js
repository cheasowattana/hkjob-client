import * as ProfileActions from '../actions/profile'

const updateProfileDetail = ( state, action ) => {
    return {
        ...state,
        ...action.profile,
        isProfileLoading: false
    }
}

const initialState = {
    isProfileLoading: false,
    isLoaded: false,
}

const profileReducer = ( state = initialState, action ) => {
    switch( action.type ) {
        case ProfileActions.PROFILE_SET: {
            return updateProfileDetail( state, action );
        }
        default: return state;
    }
}

export default profileReducer