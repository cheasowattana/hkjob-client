import axios from 'axios'
import jwt from 'jsonwebtoken'

export const LOGIN_REQUEST = 'LOGIN_REQUEST'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'

export const loginRequest = ( isAuthenticated ) => ( {
    type: LOGIN_REQUEST,
    isAuthenticated
} );

export const login = ( email, password, ownProps ) => {
    return ( dispatch ) => {
        const loginData = ( { email: email, password: password } )
        console.log( 'loginData', loginData )
        return axios.post('http://localhost:4000/login', loginData )
                    .then( loginInfo => {
                        console.log( 'loginInfo', loginInfo );
                        if( loginInfo.data.success ){
                            const decodedToken = jwt.decode( loginInfo.data.token )
                            localStorage.setItem( 'username', decodedToken.username )
                            localStorage.setItem( 'email', decodedToken.email )
                            localStorage.setItem( 'token', loginInfo.data.token )

                            dispatch( loginRequest( { isAuthenticated: true } ) )
                            ownProps.history.push( '/homepage' )
                        } else {
                            console.log('fail')
                            dispatch( loginRequest( { isAuthenticated: false } ) )
                            ownProps.history.push( '/' )
                        }
                        
                    } ).catch( err => {
                        console.log( err );
                    } );
    }
}
