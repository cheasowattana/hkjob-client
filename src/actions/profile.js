import axios from 'axios'

export const PROFILE_SET = 'PROFILE_SET'
export const PROFILE_SAVE = 'PROFILE_SAVE'

export const set = ( profile ) => ( {
    type: PROFILE_SET,
    profile
} )

export const save = () => ( {
    type: PROFILE_SAVE
} )

export const saveProfile = ( onSuccess ) => {
    return ( dispatch, getState ) => {
        console.log( getState().profile )
        return axios.post( 'http://localhost:4000/newprofile', getState().profile )
                    .then( savedProfile => {
                        //console.log( 'savedProfile', savedProfile )
                        localStorage.setItem('registerMessage', savedProfile.data.message)
                        dispatch( save() )
                        onSuccess.history.push( { pathname: '/', state: { message: savedProfile.data.message } } )
                    } )
                    .catch( err => {
                        if( err ) console.log(' FCK U, BITCH ');
                    } )
    }
}