import React from 'react'

class SignUpForm extends React.Component {

    constructor( props ) {
        super( props )
        this.state = {
            emailIsValid: false,
            passwordIsValid: false,
            confirmPasswordIsValid: false,
            usernameIsValid: false,
            formValidation: false
        }
    }

    fieldValidation = ( e ) => {
        let input = e.target.name;
        let value = e.target.value;

        switch( input ){
            case 'email': {
                let isValidEmail = value.match(
                    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                ) !== null ? true : false;
                
                this.props.updateProfile( { ...this.props.profile, email: value } )
                this.setState( { emailIsValid: isValidEmail } )
                break;
            }
            case 'password': {
                let isValidPassword = value.length >= 8 && value.length <= 20 ? true : false;

                this.props.updateProfile( { ...this.props.profile, password: value } )
                this.setState( { passwordIsValid: isValidPassword } )

                break;
            }
            case 'confirm-password': {
                let isValidConfirmPassword = value === this.props.profile.password ? true : false;

                this.props.updateProfile( { ...this.props.profile, confirmPassword: value } )
                this.setState( { confirmPasswordIsValid: isValidConfirmPassword } )

                break;
            }
            case 'username': {
                let isValidUsername = value.length >= 4 && value.length <=20 && value.match( /^([0-9]+[a-zA-Z]+|[a-zA-Z]+[0-9]+)[0-9a-zA-Z]*$/ ) !== null ? true : false;

                this.props.updateProfile( { ...this.props.profile, username: value } )
                this.setState( { usernameIsValid: isValidUsername } )

                break;
            }
            default: break;
        }
    }

    
    formValidation = ( e ) => {
        e.preventDefault();

        if( this.state.emailIsValid && this.state.passwordIsValid && this.state.confirmPasswordIsValid &&
                this.state.usernameIsValid )
            this.props.handleSubmit();

        this.setState( { formValidation: true } )
    }

    render(){
        return (
            <div className="signup-form">
                { console.log( this.props ) }
                <form onSubmit={ this.formValidation } name="frm-signup">
                    <div className="form-group">
                        <label htmlFor="email">Email Address</label>
                        <input type="email" className="form-control" name="email" onChange={ this.fieldValidation } />
                        <span>{ !this.state.formValidation ? '' : this.state.emailIsValid ? "":"Invalid E-mail format" }</span>
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input type="password" className="form-control" name="password" onChange={ this.fieldValidation } />
                        <span>{ !this.state.formValidation ? '' : this.state.passwordIsValid ? "":"Your password must be between 8 and 20 characters long." }</span>
                    </div>
                    <div className="form-group">
                        <label htmlFor="confirm-password">Confirm Pasword</label>
                        <input type="password" className="form-control" name="confirm-password" onChange={ this.fieldValidation } />
                        <span>{ !this.state.formValidation ? '' : this.state.confirmPasswordIsValid ? "":"Your confirm password doesn't match to your entered password." }</span>
                    </div>
                    <div className="form-group">
                        <label htmlFor="username">Username</label>
                        <input type="text" className="form-control" name="username" onChange={ this.fieldValidation } />
                        <span>{ !this.state.formValidation ? '' : this.state.usernameIsValid ? "":"Username has to be alphanumerics" }</span>
                    </div>
                    <button type="submit" className="btn btn-primary">
                        Register
                    </button>
                </form>
            </div>
        );
    }
}

export default SignUpForm;