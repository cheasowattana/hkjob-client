import { compose, withHandlers } from 'recompose'
import { withRouter } from 'react-router-dom'
import SignupForm from './presenter'
import { connect } from 'react-redux'

import * as ProfileActions from '../../actions/profile'

const withSubmitHandler = withHandlers({
    handleSubmit: props => () => {
        props.saveChanges( props.profile )
    }
});

const mapStateToProps = state => ( {
    profile: state.profile
} )

const mapDispatchToProps = ( dispatch, ownProps )  => ( {
    updateProfile: ( profile ) => {
        dispatch( ProfileActions.set( profile ) )
    },
    saveChanges: ( profile ) => {
        //console.log( 'profile', profile )
        dispatch( ProfileActions.saveProfile( ownProps ) )
    }
} );

export default compose(
    withRouter,
    connect( mapStateToProps, mapDispatchToProps ),
    withSubmitHandler
)(SignupForm);