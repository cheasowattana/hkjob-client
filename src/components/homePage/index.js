import { compose } from 'recompose'
import { connect } from "react-redux";
import HomePage from './presenter'

const mapStateToProps = state => ( {
    profile: state.profile,
    auth: state.auth
} );

const mapDispatchToProps = dispatch => ( {

} )

export default compose( 
    connect( mapStateToProps, mapDispatchToProps )
)( HomePage );