import React from 'react'

import './index.css'
class HomePage extends React.Component {
    render(){
        return (
            <div className="homepage">
                { console.log( this.props ) }
                <div className="logout-area">
                    <a href="/logout" className="btn btn-primary">Logout</a>
                </div>
                <h2>
                    Hello, { localStorage.getItem('username') }
                </h2>
                <p>
                    You have succesfully login.
                </p>
                <p>Welcome to HKJOB.</p>
            </div>
        );
    }
}

export default HomePage;