import React from 'react'
//import './index.css'
class LoginForm extends React.Component {

    constructor( props ){
        super( props );
        this.state = {
            email: '',
            password: '',
            emailIsValid: false,
            passwordIsValid: false,
            formValidation: false
        }
    }

    fieldValidation = ( e ) => {
        let input = e.target.name;
        let value = e.target.value;

        switch( input ){
            case 'email': {
                let isValidEmail = value.length <= 50 && value.match(
                    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                ) !== null ? true : false;
                
                //this.props.updateProfile( { ...this.props., email: value } )
                this.setState( { email: value, emailIsValid: isValidEmail } )
                break;
            }
            case 'password': {
                let isValidPassword = value.length >= 8 && value.length <= 20 ? true : false;

                //this.props.loginInfo( { ...this.props.profile, password: value } )
                this.setState( { password: value, passwordIsValid: isValidPassword } )

                break;
            }
            default: break;
        }
    }

    formValidation = ( e ) => {
        e.preventDefault();

        if( this.state.emailIsValid && this.state.passwordIsValid )
            this.props.handleSubmit( this.state.email, this.state.password );

        this.setState( { formValidation: true } )
    }

    render(){
        return (
            <div className="login-form">
                { console.log( this.props ) }
                <div style={{ color: 'red', textAlign: 'right' }}>
                    { console.log( this.props.auth.isAuthenticated ) }
                    { 
                        this.props.auth.isAuthenticated !== undefined && !this.props.auth.isAuthenticated ?
                        "Invalid Email/Password":""
                    }
                    { 
                        localStorage.getItem('registerMessage') ? localStorage.getItem('registerMessage'):'' 
                    }
                    { 
                        localStorage.getItem('registerMessage') ? localStorage.removeItem('registerMessage'):'' 
                    }
                </div>
                <form onSubmit={ this.formValidation } name="frm-login">
                    <div className="form-group">
                        <label htmlFor="email">Email Address</label>
                        <input type="email" className="form-control" name="email" onChange={ this.fieldValidation } />
                        <span>{ !this.state.formValidation ? '' : this.state.emailIsValid ? "":"Invalid E-mail format" }</span>
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input type="password" className="form-control" name="password" onChange={ this.fieldValidation } />
                        <span>{ !this.state.formValidation ? '' : this.state.passwordIsValid ? "":"Your password must be between 8 and 20 characters long." }</span>
                    </div>
                    <button type="submit" className="btn btn-primary">
                        Login
                    </button> <a href="/signup" className="btn btn-primary">Sign Up</a>
                </form>
            </div>
        );
    }

}

export default LoginForm;