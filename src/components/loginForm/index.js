import { compose, withHandlers } from 'recompose'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom' 
import LoginForm from './presenter'

import * as LoginActions from '../../actions/auth'

const loginHandler = withHandlers( {
    handleSubmit: props => ( email, password ) => {
        //console.log( 'email', email );
        //console.log( 'password', password );
        props.login( email, password )
    }
} );

const mapStateToProps = state => ( {
    auth: state.auth
} );

const mapDispatchToProps = ( dispatch, ownProps ) => ( {
    login: ( email, password ) => dispatch( LoginActions.login( email, password, ownProps ) )
} );

export default compose( 
    withRouter,
    connect( mapStateToProps, mapDispatchToProps ),
    loginHandler
)( LoginForm );