First, install the npm packages for the project

        # yarn install

Then, you can start the project with:

        # yarn start

Make sure you have running the server side service already.

5 working days is not enough for me to write up the Unit Tests, since I have been so busy with my projects as well. Frankly, I have been rarely working on writing tests, but I have worked with jest.js before. Still, in a limited time, I missed to write it for you.